package animals;

public interface Animals {

    /**
     * Метод, возвращает название животного
     * @return (String) название животного
     */
    String getName();

    /**
     * Метод возвращает голос(слова\говор) животного
     * @return (String) голос(слова\говор) животного
     */
    String getVoice();
}
