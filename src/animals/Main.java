package animals;

public class Main {
    public static void main(String... args) {
        Cat cat=new Cat();
        Dog dog=new Dog();
        Pig pig = new Pig();
        Chicken chicken = new Chicken();
        Frog frog=new Frog();
        Cow cow=new Cow();
        System.out.println(cat.getName()+ " сказал "+ cat.getVoice());
        System.out.println(dog.getName()+ " сказал "+ dog.getVoice());
        System.out.println(pig.getName()+ " сказал " + pig.getVoice());
        System.out.println(chicken.getName() + " сказал " + chicken.getVoice());
        System.out.println(cat.getName()+ " сказал "+ cat.getVoice());
        System.out.println(dog.getName()+ " сказал "+ dog.getVoice());
        System.out.println(frog.getName()+ " сказал "+ frog.getVoice());
        System.out.println(cow.getName()+ " сказал "+ cow.getVoice());

    }
}
