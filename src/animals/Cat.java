package animals;

public class Cat implements Animals {
    @Override
    public String getName() {
        return "Гарфилд";
    }

    @Override
    public String getVoice() {
        return "мяу-мяу-мяу";
    }
}
