package animals;

/**
 * Created by LEONID on 14.10.2014.
 */
public class Chicken implements Animals{
    @Override
    public String getName() {
        return "Цыпленок";
    }

    @Override
    public String getVoice() {
        return "Цып-цып";
    }
}
