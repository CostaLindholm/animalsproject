package animals;

public class Cow implements Animals{
    @Override
    public String getName() {
        return "Корова";
    }

    @Override
    public String getVoice() {
        return "му-му-му";
    }
}
