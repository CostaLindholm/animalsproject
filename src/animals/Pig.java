package animals;

/**
 * Created by LEONID on 14.10.2014.
 */
public class Pig implements Animals {
    @Override
    public String getName() {
        return "Поросенок";
    }

    @Override
    public String getVoice() {
        return "Хрю-Хрю";
    }
}
